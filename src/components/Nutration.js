import React from 'react';
import DailyReturn from './Nutration/DailyReturn';
import Apr from './Nutration/APR';
import DevFee from './Nutration/Devfee';
const Nutration = ()=>{
    return(
        <>
        <div id="nutration">
        <h3>Nutration Facts</h3>
          
          <div id="nutration_texts">
              <p className="nutra_para">
                  <span>Daily Return</span>  <span> <DailyReturn/>%</span>
              </p>

              <p className="nutra_para">
                  <span>APR</span>  <span> <Apr/>%</span>
              </p>


              <p className="nutra_para">
                  <span>Dev Fee</span>  <span> <DevFee/>%</span>
              </p>
          </div>

        </div>
        
        </>
    )
}


export default Nutration;