import React from 'react';
import 'react-perfect-scrollbar/dist/css/styles.css';
import PerfectScrollbar from 'react-perfect-scrollbar';
import Logo from './Logo';
import Mainbox from './Main_box';
import Nutration from './Nutration';
import RefBox from './RefBox';
import Social from './Social';
const Main = ()=>{

 
    return(
        <>

        <PerfectScrollbar>

             
        <div  id="main">
            <div id="subMain">
            <Logo/>
            <Mainbox/>
            <Nutration/>
            <RefBox/>
            <Social/>
                </div>
       
         

          </div>

        </PerfectScrollbar>

        
        
        </>
    )
}


export default Main