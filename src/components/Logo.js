import React from 'react';
import Log  from './logo.png';

const Logo = ()=>{
    return(
        <>
        <img id="log"  src={Log} alt="logo"/>
        <h4 class="bnb_text">
        The BNB Reward Pool with the tastiest daily return and lowest dev fee
        </h4>
        </>
    )
}


export default Logo;