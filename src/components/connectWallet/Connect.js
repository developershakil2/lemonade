import React, { useState } from 'react';
import Meta from './metamask.png';
import Wallet from './wallet.png';
const Connect = ()=>{
      const close = 'none';
      const open = 'grid';

    const [connectWrapper, setConnectWrapper] = useState(close);
    const openFunc = ()=>{
        setConnectWrapper(close);
    }
    const openBtn = ()=>{
        setConnectWrapper(open);
    }
    return(
        <>
        <button onClick={openBtn} id="connect">CONNECT</button>





        <div style={{display:connectWrapper}} onClick={openFunc} id="connect_div_wrapper">
            <div id="connect_wallet_div">
                <button className="connect_wallet">
                    <img src={Meta} alt="meta"/>
                    <h3>MetaMask</h3>
                    <small>Connect to your MetaMask Wallet</small>
                </button>
                
                <button id="scan_wallet" className="connect_wallet">
                    <img src={Wallet} alt="meta"/>
                    <h3>WalletConnect</h3>
                    <small>Scan with WalletConnect to connect</small>
                </button>
                </div>
        </div>
        </>
    )
}

export default Connect;