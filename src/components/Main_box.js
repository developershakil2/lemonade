import React from 'react';
import BnbInput from './BoxInputfields/BnbInputValue';
import LemonadeBtn from './boxButtons/LemonadeBtn';
import Reward from './yourRewared/YourReward';
import ReBake from './boxButtons/ReBakeBtn';
import EatBeans from './boxButtons/EatBeansBtn';
import Connect from './connectWallet/Connect';


const MainBox = ()=>{
    return(
        <>
    <div id="connect_mobile">
    <Connect/>
    </div>

        <div id="main_box">
            <div id="main_box_texts">
                <h3 className="main_box_top_texts">
                <span>Contract</span>  <span>0 BNB</span>

                </h3>
                <h3 className="main_box_top_texts">
                <span>Wallet</span>  <span>0 BNB</span>

                </h3>
                <h3 id="yourbeans" className="main_box_top_texts">
                <span>Your BEANS</span>  <span>0 BEANS</span>

                </h3>
            </div>

            <div className="bnbinput">
                <BnbInput/> <h2>BNB</h2>
            </div>

            <div id="lamonadebtn" className="bnbinput">
             <LemonadeBtn/>
            </div>


            <div id="reward_div">
               <Reward/>
            </div>


            <div id="main_box_bottom_btn">
             <ReBake/>

            <EatBeans/>

            </div>

           
        </div>
        </>
    )
}


export default MainBox;