import React from 'react';
import Main from './components/Main';
import Connect from './components/connectWallet/Connect';
function App() {
  return (
    <>
    <Main/>
     <div id="connect_desktop">
       <Connect/>
     </div>
    </>
  );
}

export default App;
